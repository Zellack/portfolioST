let circle_score = '';
circle_score = document.getElementsByTagName('circle');

// document.addEventListener("DOMContentLoaded", function (event) {
//     resizer(() => {
//         let aIcons = document.getElementsByClassName('Icon');
//         for (let i = 0; i < aIcons.length; i++) {
//             aIcons[i].style.animationPlayState = 'running';
//         }
//     });
// });

if (document.readyState !== 'loading') {
    resizer(function () {
      var aIcons = document.getElementsByClassName('Icon');
  
      for (var i = 0; i < aIcons.length; i++) {
        aIcons[i].style.animationPlayState = 'running';
      }
    });
  }
  
function resizer(cb) {
    if (window.matchMedia('(max-width: 650px)').matches) {
        /* La largeur minimum de l'affichage est 650 px inclus */
        for (let i = 0; i < circle_score.length; i++) {
            let wh = circle_score[i].getAttribute("data-wh");
            let percent = circle_score[i].getAttribute("data-percent");
            let r = (wh / 2.5) / 2;

            if (circle_score[i].className.animVal === 'score') circle_score[i].setAttribute('stroke-dasharray', `${(percent * (2 * r) * Math.PI) / 100}, 999`);
            circle_score[i].setAttribute('r', `${r}px`);
            circle_score[i].setAttribute('stroke-width', 4);
        }
    } else {
        /* L'affichage est inférieur à 600px de large */
        if (window.matchMedia('(max-width: 1200px)').matches) {
            /* La largeur minimum de l'affichage est 600 px inclus */
            for (let i = 0; i < circle_score.length; i++) {
                let wh = circle_score[i].getAttribute("data-wh");
                let percent = circle_score[i].getAttribute("data-percent");
                let r = (wh / 1.5) / 2;

                if (circle_score[i].className.animVal === 'score') circle_score[i].setAttribute('stroke-dasharray', `${(percent * (2 * r) * Math.PI) / 100}, 999`);
                circle_score[i].setAttribute('r', `${r}px`);
                circle_score[i].setAttribute('stroke-width', 6);
            }
        } else {
            for (let i = 0; i < circle_score.length; i++) {
                let wh = circle_score[i].getAttribute("data-wh");
                let percent = circle_score[i].getAttribute("data-percent");
                let r = wh / 2;

                if (circle_score[i].className.animVal === 'score') circle_score[i].setAttribute('stroke-dasharray', `${(percent * (2 * r) * Math.PI) / 100}, 999`);
                circle_score[i].setAttribute('r', `${r}px`);
                circle_score[i].setAttribute('stroke-width', 8);
            }
        }
    }
    if (typeof (cb) === 'function') cb();
}

window.addEventListener('resize', resizer, false);