var footer = document.querySelector('footer');
var logos = footer.querySelectorAll('a');

document.addEventListener("DOMContentLoaded", function (event) {
    for (var i = 0; i < logos.length; i++) {
        logos[i].addEventListener('mouseover', function () {
            for (var i = 0; i < logos.length; i++){
                if (logos[i] === this) logos[i].parentNode.classList.add('logoactive');
                else logos[i].parentNode.classList.remove('logoactive');
            }
        }, false);
    }
});