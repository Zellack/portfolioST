"use strict";

var circle_score = '';
circle_score = document.getElementsByTagName('circle'); // document.addEventListener("DOMContentLoaded", function (event) {
//     resizer(() => {
//         let aIcons = document.getElementsByClassName('Icon');
//         for (let i = 0; i < aIcons.length; i++) {
//             aIcons[i].style.animationPlayState = 'running';
//         }
//     });
// });

if (document.readyState !== 'loading') {
  resizer(function () {
    var aIcons = document.getElementsByClassName('Icon');

    for (var i = 0; i < aIcons.length; i++) {
      aIcons[i].style.animationPlayState = 'running';
    }
  });
}

function resizer(cb) {
  if (window.matchMedia('(max-width: 650px)').matches) {
    /* La largeur minimum de l'affichage est 650 px inclus */
    for (var i = 0; i < circle_score.length; i++) {
      var wh = circle_score[i].getAttribute("data-wh");
      var percent = circle_score[i].getAttribute("data-percent");
      var r = wh / 2.5 / 2;
      if (circle_score[i].className.animVal === 'score') circle_score[i].setAttribute('stroke-dasharray', "".concat(percent * (2 * r) * Math.PI / 100, ", 999"));
      circle_score[i].setAttribute('r', "".concat(r, "px"));
      circle_score[i].setAttribute('stroke-width', 4);
    }
  } else {
    /* L'affichage est inférieur à 600px de large */
    if (window.matchMedia('(max-width: 1200px)').matches) {
      /* La largeur minimum de l'affichage est 600 px inclus */
      for (var _i = 0; _i < circle_score.length; _i++) {
        var _wh = circle_score[_i].getAttribute("data-wh");

        var _percent = circle_score[_i].getAttribute("data-percent");

        var _r = _wh / 1.5 / 2;

        if (circle_score[_i].className.animVal === 'score') circle_score[_i].setAttribute('stroke-dasharray', "".concat(_percent * (2 * _r) * Math.PI / 100, ", 999"));

        circle_score[_i].setAttribute('r', "".concat(_r, "px"));

        circle_score[_i].setAttribute('stroke-width', 6);
      }
    } else {
      for (var _i2 = 0; _i2 < circle_score.length; _i2++) {
        var _wh2 = circle_score[_i2].getAttribute("data-wh");

        var _percent2 = circle_score[_i2].getAttribute("data-percent");

        var _r2 = _wh2 / 2;

        if (circle_score[_i2].className.animVal === 'score') circle_score[_i2].setAttribute('stroke-dasharray', "".concat(_percent2 * (2 * _r2) * Math.PI / 100, ", 999"));

        circle_score[_i2].setAttribute('r', "".concat(_r2, "px"));

        circle_score[_i2].setAttribute('stroke-width', 8);
      }
    }
  }

  if (typeof cb === 'function') cb();
}

window.addEventListener('resize', resizer, false);