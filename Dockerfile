# pull official base image
FROM node:16-alpine

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
#ENV PATH /app/node_modules/.bin:$PATH

# add app
COPY . .

# install app dependencies
# COPY package.json ./
# COPY package-lock.json ./
RUN npm install

RUN npm install -g serve

RUN npm run build

# start app
CMD ["serve", "-s", "build"]
